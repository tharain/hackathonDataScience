from django.conf.urls import url
from django.contrib import admin
from DataScienceApp.views.uploadfile import UploadFileView
from DataScienceApp.views.graphdisplay import GraphDisplayView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', UploadFileView.as_view(), name='upload'),
    url(r'^graph/$', GraphDisplayView.as_view(), name='graph'),
]
