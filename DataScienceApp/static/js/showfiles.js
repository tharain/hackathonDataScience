var selDiv = "";

document.addEventListener("DOMContentLoaded", init, false);

function init() {
    $("#upload-file1").click(function() {
        document.querySelector('#id_data_file_upload').addEventListener('change', handleFileSelect, false);
        selDiv = document.querySelector("#selectedQuestionFiles");
    });
}

function handleFileSelect(e) {

    if (!e.target.files) return;

    selDiv.innerHTML = "";

    var files = e.target.files;
    for (var i = 0; i < files.length; i++) {
        var f = files[i];

        selDiv.innerHTML += f.name + "<br/>";

    }
}