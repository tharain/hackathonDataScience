from DataScienceApp.forms.uploadfile import UploadFileForm
from django.http import HttpResponseRedirect
from django.views.generic import FormView

class UploadFileView(FormView):
    form_class = UploadFileForm
    template_name = 'DataScienceApp/index.html'
    success_url = '/'

    def post(self, request, *args, **kwargs):
        upload_file_form = self.form_class(request.POST, request.FILES)

        if upload_file_form.is_valid():
            data_file = request.FILES['data_file_upload']

            if data_file is None:
                raise ValueError("No Upload for Q&A file or Data Files")

            print "========"
            print data_file

            return HttpResponseRedirect("../DataScienceApp/graph")