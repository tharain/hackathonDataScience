from django.views.generic import TemplateView

class GraphDisplayView(TemplateView):
    template_name = 'DataScienceApp/graphdisplay.html'