# main.py - This is for testing backend part

from constants import CONST
from preprocess import whiten, identify
import os.path
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image

# read path setting
def read_settings():
	pathFileName = CONST.path_config_file

	if(os.path.exists(pathFileName) == False):
		tf = open(pathFileName, "a+")		
		item = raw_input("Where are the datasets? ")
		item = str(item) + "\n"
		tf.write(item)
		tf.close()

	with open(pathFileName, "r") as pathFile:
		CONST.eval_dataset_path = pathFile.readline().strip() # set 1st line as the path for dataset
		pathFile.close()
		
def show_image(img, processed):
	plt.figure(".::Image::.")
	plt.imshow(img)
	plt.figure(".::Processed::.")
	plt.imshow(processed)
	plt.show()
		
def read_datas_path():
	dataPathFileName = "dataPath.txt"
	
	dataPaths = []
	ffile = open(dataPathFileName, "r")

	# Read the files
	for line in ffile:
		dataPaths.append(line)
	
	ffile.close()
	return dataPaths

if __name__ == "__main__": # main function
	read_settings()
	
	print("Preparing Data")
	
	imagesDatas = read_datas_path()
	for imageData in imagesDatas:
		path = os.path.join(CONST.eval_dataset_path, imageData.rstrip())
		img = Image.open(path)
		arr = np.array(img)
		preprocess = whiten(arr)
		show_image(arr, preprocess)