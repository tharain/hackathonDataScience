# constants.py

import os.path

def constants(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        return f()
    return property(fget, fset)

class _Constants(object):
	eval_dataset_path = ""

	@constants
	def path_config_file():
		return "path.txt"	
	def data_path_file():
		return "dataPath.txt"

CONST = _Constants()