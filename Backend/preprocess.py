def identify(arr):
	height = len(arr)
	width = len(arr[0])
	
	temp = list()
	lines = list()
	rectangles = list()
	
	a = 0
	b = 0
	c = height
	d = width
	
	if(c - a > 0 and d - b > 0):
		temp = list()
		l = a
		k = b
		while(l + 1 < c and k + 1 < d):
			if(arr[l][k][0] == 40):
				temp.append([l,k])
			l = l + 1
			k = k + 1
		l = a
		k = d - 1
		while(l + 1 < c and k > b):
			if(arr[l][k][0] == 40):
				temp.append([l,k])
			l = l + 1
			k = k - 1
		l = a
		k = (b + d) / 2
		while(l + 1 < c):
			if(arr[l][k][0] == 40):
				temp.append([l,k])
			l = l + 1
		l = (a + c) / 2
		k = b
		while(k + 1 < d):
			if(arr[l][k][0] == 40):
				temp.append([l,k])
			k = k + 1
		#if(len(temp) == 0):
		#	break;
		for i in range(0, len(temp)):
			coord = temp[i]
			for j in range(i, len(temp)):
				if(i == j):
					continue
				if(coord[0] == temp[j][0]):
					lines.append([coord[0],b,coord[0],d])
				if(coord[1] == temp[j][1]):
					lines.append([a,coord[1],c,coord[1]])
		for i in range(0, len(lines)):
			line = lines[i]
			index = 0
			for j in range(i, len(lines)):
				if(i == j):
					continue
				gradient1 = 0
				c1 = line[1]
				gradient2 = 0
				c2 = lines[j][1]
				if(line[2] - line[0] > 0):
					gradient1 = (line[3] - line[1]) / line[2] - line[0]
					if(gradient1 * line[0] > 0):
						c1 = line[1] / (gradient1 * line[0])
				if(lines[j][2] - lines[j][0] > 0):
					gradient2 = (lines[j][3] - lines[j][1]) / lines[j][2] - lines[j][0]
					if(gradient1 * line[0] > 0):
						c2 = 1.0 * line[1] / (gradient1 * line[0])
				if(gradient1 - gradient2 > 0):
					ptx = (c2 - c1) / (gradient1 - gradient2)
					pty = gradient1 * ptx + c1
					print(ptx + ", " + pty)
	return 1
				
				

def whiten(arr):	# Attempts to cleanup the background especially for gif images
	# check for gif images or jpeg image
	height = len(arr)
	width = len(arr[0])
	off = 2

	TrialArr = [[[0,0,0] for x in range(0,width)] for y in range(0,height)]
	try:
		arr[0][0][0] < 256
		for i in range(0,height):
			for j in range(0,width):
				currPixel = arr[i][j]
	except IndexError:
		for i in range(0,height):
			for j in range(0,width):
				currPixel = arr[i][j]
				count = [0 for x in range(0,9)]
				if(j - off >= 0):
					if(arr[i][j - off] == currPixel):
						count[3] = 1;
					elif(arr[i][0] == currPixel):
						count[3] = 1;
				if(j + off < width):
					if(arr[i][j + off] == currPixel):
						count[5] = 1;
					elif(arr[i][width-1] == currPixel):
						count[5] = 1;
				
				if(i + off < height):
					if(j - off >= 0):
						if(arr[i + off][j - off] == currPixel):
							count[6] = 1;
						elif(arr[i + off][0] == currPixel):
							count[6] = 1
					if(arr[i + off][j] == currPixel):
						count[7] = 1;
					if(j + off < width):
						if(arr[i + off][j + off] == currPixel):
							count[8] = 1;
						elif(arr[i + off][width-1] == currPixel):
							count[8] = 1;
				else:
					if(j - off >= 0):
						if(arr[height-1][j - off] == currPixel):
							count[6] = 1;
						elif(arr[height-1][0] == currPixel):
							count[6] = 1
					if(arr[height-1][j] == currPixel):
						count[7] = 1;
					if(j + off < width):
						if(arr[height-1][j + off] == currPixel):
							count[8] = 1;
						elif(arr[height-1][width-1] == currPixel):
							count[8] = 1;
				
				if(i - off >= 0):
					if(j - off >= 0):
						if(arr[i - off][j - off] == currPixel):
							count[0] = 1;
						elif(arr[i - off][0] == currPixel):
							count[0] = 1
					if(arr[i - off][j] == currPixel):
						count[1] = 1;
					if(j + off < width):
						if(arr[i - off][j + off] == currPixel):
							count[2] = 1;
						elif(arr[i - off][width-1] == currPixel):
							count[2] = 1;
				else:
					if(j - off >= 0):
						if(arr[0][j - off] == currPixel):
							count[0] = 1;
						elif(arr[i + off][0] == currPixel):
							count[0] = 1
					if(arr[0][j] == currPixel):
						count[1] = 1;
					if(j + off < width):
						if(arr[0][j + off] == currPixel):
							count[2] = 1;
						elif(arr[0][width-1] == currPixel):
							count[2] = 1;
				
				if(count[1] and count[5]): # L shape
					TrialArr[i][j] = [40, 40, 40]
				elif(count[1] and count[3]): # _| shape
					TrialArr[i][j] = [40, 40, 40]
				elif(count[5] and count[7]): # |- shape
					TrialArr[i][j] = [40, 40, 40]
				elif(count[3] and count[7]): # -| shape
					TrialArr[i][j] = [40, 40, 40]
				elif(count[1] and count[3] and count[5] and count[7]): # + shape
					TrialArr[i][j] = [40, 40, 40]
				else:
					TrialArr[i][j] = [255, 255, 255]
	return TrialArr
			